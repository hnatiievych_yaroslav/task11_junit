package com.hnatiievych.view;

import com.hnatiievych.Application;
import com.hnatiievych.controller.ControllerLongestPlateau;
import com.hnatiievych.controller.ControllerMinesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private ControllerLongestPlateau controllerPlateau;
    private ControllerMinesweeper controllerMinesweeper;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(Application.class);

    public MyView() {
        controllerPlateau = new ControllerLongestPlateau();
        controllerMinesweeper = new ControllerMinesweeper();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Test longest plateau");
        menu.put("2", "  2 - Test Minesweeper");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        logger.info("Test longest plateau.");
        int[] array = new int[]{1, 2, 3, 4, 4, 4, 3, 2, 2, 1, 5, 5, 5, 5, 2, 1, 1, 1, 1, 1, 0};

        logger.info("we have array: " + Arrays.toString(array));
        logger.info("The length of longest plateau is: " + controllerPlateau.lengthArrayEqualsNumber(array));
        logger.info("This plateau started with " + controllerPlateau.possitionStartarrayEqualsNumber(array) + " index");

    }

    private void pressButton2() {
        logger.info("Play Minesweeper");
        logger.info("Enter higth board");
        int sizeM = input.nextInt();
        logger.info("Enter wigth board");
        int sizeN = input.nextInt();
        logger.info("size probability");
        int probability = input.nextInt();
        logger.info(controllerMinesweeper.createGame(sizeM, sizeN, probability));
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
