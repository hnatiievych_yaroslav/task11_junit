package com.hnatiievych.model;

public class LongestPlateau {


    public static int lengthArrayEqualsNumber(int[] array) {
        int maxLength = 0;
        int curLength = 1;
        int endPossitionIndex = 0;
        int endIndex = 0;


        for (int i = 1; i < array.length - 2; i++) {
            endIndex = array.length - 1;
            curLength = 1;
            if (array[i] == array[i + 1] & array[i] > array[i - 1]) {
                for (int k = i; k < endIndex; k++) {
                    if (array[k] == array[k + 1]) {
                        curLength++;
                    } else {
                        if (array[k] > array[k + 1]) {
                            if (curLength > maxLength)
                                maxLength = curLength;
                            endIndex = k;
                            endPossitionIndex = k;
                        } else {
                            curLength = 0;
                        }
                    }
                }
            }
        }
        return maxLength;
    }

    public static int possitionStartArrayEqualsNumber(int[] array) {
        int maxLength = 0;
        int curLength = 1;
        int endPossitionIndex = 0;
        int endIndex = 0;


        for (int i = 1; i < array.length - 2; i++) {
            endIndex = array.length - 1;
            curLength = 1;
            if (array[i] == array[i + 1] & array[i] > array[i - 1]) {
                for (int k = i; k < endIndex; k++) {
                    if (array[k] == array[k + 1]) {
                        curLength++;
                    } else {
                        if (array[k] > array[k + 1]) {
                            if (curLength > maxLength)
                                maxLength = curLength;
                            endIndex = k;
                            endPossitionIndex = k;
                        } else {
                            curLength = 0;
                        }
                    }
                }
            }
        }
        return endPossitionIndex-maxLength+1;
    }

}
