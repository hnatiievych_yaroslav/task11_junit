package com.hnatiievych.model;

import java.util.Random;

public class Minesweeper {

    boolean[][] board;
    private int sideM;
    private int sideN;
    private int probably;

    public Minesweeper(int sideM, int sideN, int probably) {
        this.sideM = sideM;
        this.sideN = sideN;
        this.probably = probably;
        this.board = createMinesweeperBoard();
    }

    private boolean[][] createMinesweeperBoard() {
        boolean[][] board = new boolean[sideM][sideN];
        for (int i = 0; i < sideM; i++) {
            for (int j = 0; j < sideN; j++) {
                board[i][j] = createMine(probably);
            }
        }
        return board;
    }


    public String printBomb() {
        StringBuilder print = new StringBuilder();
        print.append("\n");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == true)
                    print.append(" *");
                else print.append(" .");
            }
            print.append("\n");
        }
        return print.toString();
    }

    public String printHideBomd() {
        StringBuilder print = new StringBuilder();
        print.append("\n");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == true)
                    print.append(" *");
                else print.append(" "+calculateMinesAround(i, j));
            }
            print.append("\n");
        }
        return print.toString();
    }


    private int calculateMinesAround(int i, int j) {
        int minesAround = 0;
        if (board[i][j] != true) {

            if (i - 1 >= 0 & j - 1 >= 0) {
                if (board[i - 1][j - 1] == true) {
                    minesAround++;
                }
            }
            if (i - 1 >= 0 & j + 1 < board[i].length) {
                if (board[i - 1][j] == true) {
                    minesAround++;
                }
                if (board[i - 1][j + 1] == true) {
                    minesAround++;
                }
            }
            if (i + 1 < board.length & j - 1 >= 0) {
                if (board[i][j - 1] == true) {
                    minesAround++;
                }
                if (board[i + 1][j - 1] == true) {
                    minesAround++;
                }
            }
            if (j + 1 < board[i].length) {
                if (board[i][j + 1] == true) {
                    minesAround++;
                }
            }
            if (i + 1 < board.length) {
                if (board[i + 1][j] == true) {
                    minesAround++;
                }
            }
            if (i + 1 < board.length & j + 1 < board[i].length)
                if (board[i + 1][j + 1] == true) {
                    minesAround++;
                }
        }
        return minesAround;
    }

    private boolean createMine(int probably) {
        Random random = new Random();
        int size = sideM * sideN;
        if (random.nextInt(size) > size * probably / 100) {
            return false;
        }
        return true;
    }

}
