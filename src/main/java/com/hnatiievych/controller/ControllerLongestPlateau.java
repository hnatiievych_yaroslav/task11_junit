package com.hnatiievych.controller;

import com.hnatiievych.model.LongestPlateau;

public class ControllerLongestPlateau {

    public int lengthArrayEqualsNumber(int[] array) {
        return LongestPlateau.lengthArrayEqualsNumber(array);
    }

    public int possitionStartarrayEqualsNumber(int[] array) {
        return LongestPlateau.possitionStartArrayEqualsNumber(array);
    }
}
