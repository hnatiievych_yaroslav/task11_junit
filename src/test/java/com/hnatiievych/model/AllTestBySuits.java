package com.hnatiievych.model;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MinesweeperTest.class, LongestPlateauTest.class})
public class AllTestBySuits {

}
