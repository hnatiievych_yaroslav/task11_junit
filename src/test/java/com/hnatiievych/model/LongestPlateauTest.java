package com.hnatiievych.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class LongestPlateauTest {
    private static int [] array;

    @BeforeClass
    public static void init(){
        array = new int[]{1,2,3,4,5,5,5,4,3,3,3,1};
    }


    @Test
    public void lengthArrayEqualsNumber() {
        int actual = LongestPlateau.lengthArrayEqualsNumber(array);
        Assert.assertEquals(3,actual);
    }

    @Test
    public void possitionStartArrayEqualsNumber() {
        int actual = LongestPlateau.possitionStartArrayEqualsNumber(array);
        Assert.assertEquals(4,actual);
    }
}